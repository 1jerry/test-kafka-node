# Documentation for Docker Kafka

-	From site: https://www.svds.com/data-ingestion-spark-kafka/
-	Summary (only parts of the instructions are necessary)
-	We only need Docker.  If you want to process Tweets with your Kafka installation, then you can do all the other steps also.

1. 	[Install](https://www.docker.com/get-docker)
1. 	Commands to run from Terminal
    1. JTBC (Just To Be Clear) the first part of the commands below, ending with “$” or “#” are the command prompt, not what you type.
1. 	Create local docker:
    1. $ docker pull spotify/kafka
1. 	Start, initialize, and name the image:
    1. $ docker run -p 2181:2181 -p 9092:9092 --hostname kafka --name test_kafka --env ADVERTISED_PORT=9092 --env ADVERTISED_HOST=kafka spotify/kafka
1. 	Start the image
    1. ^C, wait for the image to stop
    1. $ docker start test_kafka
1. 	Connect to image
    1. $ docker start test_kafka
    1. your command prompt show now look like: root@kafka:/#
    1. these commands will also work from your host, if you have Kafka installed there also.
1. 	create topic test_topic
    1. root@kafka:/# /opt/kafka_2.11-0.10.1.0/bin/kafka-topics.sh --zookeeper kafka:2181 --create --topic test_topic --partitions 3 --replication-factor 1
1. 	verify the topic exists
    1. root@kafka:/# /opt/kafka_2.11-0.10.1.0/bin/kafka-topics.sh --zookeeper kafka:2181 --list
1. 	Push messages to the topic
    1. root@kafka:/# /opt/kafka_2.11-0.10.1.0/bin/kafka-console-producer.sh --topic test_topic --broker-list kafka:9092
    1. Type messages in
    1. ^C to end
1. 	Play back messages from topic
    1. Can be in another Terminal instance. 
    1. root@kafka:/# /opt/kafka_2.11-0.10.1.0/bin/kafka-console-consumer.sh --topic test_topic --bootstrap-server kafka:9092 --from-beginning
    1. again, ^C to end
1. 	play back topic programmatically.
    1. $ node consumer.js --topic=test_topic --offset=117
    1. Topic name is required
    1. Offset is optional.  Defaults to end  (only items created after you start will show up)
1. 