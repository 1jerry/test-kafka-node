'use strict';

var kafka = require('kafka-node');
var Consumer = kafka.Consumer;
var Offset = kafka.Offset;
var Client = kafka.KafkaClient;
var argv = require('optimist').argv;
var topic = argv.topic || 'topic1';
var startWith = argv.offset || 999

var client = new Client({ kafkaHost: 'localhost:9092' });
var topics = [{ topic: topic, partition: 0 ,offset:startWith}];

// var topics = [{ topic: topic, partition: 1, offset:99 }, { topic: topic, partition: 0 ,offset:99}];
var options = { fromOffset: true, autoCommit: false, fetchMaxWaitMs: 1000, fetchMaxBytes: 1024 * 1024 };

var consumer = new Consumer(client, topics, options);
var offset = new Offset(client);

consumer.on('message', function (message) {
  console.log(message);
});

consumer.on('error', function (err) {
  console.log('error', err);
});

/*
* If consumer get `offsetOutOfRange` event, fetch data from the end
*/
consumer.on('offsetOutOfRange', function (topic) {
  offset.fetchLatestOffsets([topic.topic], function (err, offsets) {
    if (err) {
      return console.error(err);
    }
    var min = offsets[topic.topic][topic.partition]
    // console.log('fetchLATEST: Out of range.  Set to ',min)
    consumer.setOffset(topic.topic, topic.partition, min);
  })
});