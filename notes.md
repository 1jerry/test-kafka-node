# Kafka commands.  Run from dev/examples/Kafka
NOT WORKING
## start Zookeeper
``` bash
 bin/zookeeper-server-start.sh config/zookeeper.properties
```
## start Kafka
```
bin/kafka-server-start.sh config/server.properties
```
## Create topic _node-test_ (one time)
```
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic node-test node-test
```
## verify that our topic was created correctly
```
bin/kafka-topics.sh --describe --zookeeper localhost:2181 --topic node-test
```

## testing results

* Zookeeper and Kafka seem to start correctly, verify works
* running producer.js will eventially time-out plus:
    * Zookeeper will start opening and closing ports continously with "no session established"
    * Kafka will **not** exit with ^C if you close Zookeeper first!
    