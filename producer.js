var kafka = require('kafka-node'),
    Producer = kafka.HighLevelProducer,
    client = new kafka.KafkaClient({
        kafkaHost: 'localhost:2181',
        requestTimeout: 300
    }),  // defaults to port 9092
    producer = new Producer(client)
var type = require('./schema').type


client.on('error', function(error) {
    console.error(error);
})
    
producer.on('ready', function() {
    // create message and encode buffer
    var messageBuffer = type.toBuffer({
        enumField: 'sym1',
        id: '3kj2k1l',
        timestamp: Date.now()
    })
    // create a payload
    var payload = [{
        topic: 'node-test',
        messages: messageBuffer,
        attributes: 1
    }]
    // send payload to Kafka
    producer.send(payload, function(error, result) {
        console.info('Sent payload to Kafka: ', payload)
        if (error) {
            console.error(error)
        } else {
            var formattedResult = result[0]
            console.log('result: ',result);
        }
    })
})
producer.on('error', function(error) {
    console.error(error);
})